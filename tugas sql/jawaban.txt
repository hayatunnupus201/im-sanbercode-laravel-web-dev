1. Buat Database
CREATE DATABASE myshop;

2. Membuat Table di Dalam Database
TABLE USERS

CREATE TABLE users (
	id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) not null,
    email varchar(255) not null,
    password varchar(255) not null

);

TABLE CATEGORIES

CREATE TABLE categories(
	id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)

);

TABLE ITEMS

CREATE TABLE items(
	id int PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) not null,
    description varchar(255) not null,
    price int,
    stock int,
    category_id int not null,
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

3. Memasukkan data pada table
users

INSERT INTO users (name, email, password) VALUES 
("John Doe", "john@doe.com", "john123"),
("Jane Doe", "jane@doe.com", "jenita123")

categories

INSERT INTO categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded")

items

INSERT INTO items(name, description, price, stock, category_id) VALUES
                  ("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),
                  ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),
                  ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1)

4. Mengambil data dari database
a. mengambil data users
SELECT id, name, email FROM users;

b. mengambil data items
- query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
SELECT * FROM `items` WHERE price > 1000000;

-query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci �uniklo�, �watch�, atau �sang� (pilih salah satu saja).
SELECT * FROM `items` WHERE name LIKE "uniklo%";

c. menampilkan data items join dengan kategori
SELECT items.*, categories.name AS kategori FROM items INNER JOIN categories ON items.category_id = categories.id;

5. mengubah data dari database
UPDATE items SET price = 2500000 WHERE name = "sumsang b50";