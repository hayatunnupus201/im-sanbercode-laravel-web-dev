@extends('layout.master')
@section('title')
    Buat Account Baru!
@endsection
@section('sub-title')
    Sign Up Form
@endsection
@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label >First name:</label> <br><br>
        <input type="text" name="fname"><br><br>
        <label >Last Name:</label> <br><br>
        <input type="text" name="lname"><br><br>
        <label >Gender:</label><br><br>
        <input type="radio" name="Gender">Male<br>
        <input type="radio" name="Gender">Female<br>
        <input type="radio" name="Gender">Other<br><br>
        <label >Nationality:</label><br><br>
        <select name="Nationality">
            <option value="">Indonesia</option>
            <option value="">Singapore</option>
            <option value="">American</option>
        </select><br><br>
        <label >Language Spoken:</label><br><br>
        <input type="checkbox" name="language spoken">Bahasa Indonesia <br>
        <input type="checkbox" name="language spoken">English <br>
        <input type="checkbox" name="language spoken">Other<br><br>
        <label >Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection
    