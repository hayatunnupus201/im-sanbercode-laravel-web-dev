@extends('layout.master')
@section('title')
    Edit Data Pemain 
@endsection
@section('sub-title')
    Isi Data Dibawah Ini
@endsection
@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
        </div>
        @error('exampleInputName')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label>Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
        </div>
         @error('exampleInputUmur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10">value="{{$cast->bio}}"</textarea>
        </div>
         @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection