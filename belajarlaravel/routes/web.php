<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController:: class , 'utama']);
Route::get('/form', [AuthController:: class , 'regis']);

Route::post('/welcome', [AuthController:: class , 'send']);

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-table', function(){
    return view('page.data-table');
});

//crud cast

//rute untuk menampilkan form data pemain baru
Route::get('/cast/tambah', [CastController:: class , 'create']);

//rute untuk menyimpan data ke cast
Route::post('/cast', [CastController:: class , 'store']);

//read data
//menampilkan semua data yg ada di db
Route::get('/cast', [CastController:: class, 'index']);
//menampilkan detail data pemain film dengan id tertentu
Route::get('/cast/{id}', [CastController:: class, 'show']);    

//update data
Route::get('/cast/{id}/edit', [CastController:: class, 'edit']);
Route::put('/cast/{id}', [CastController:: class, 'update']);

//delate data
Route::delete('/cast/{id}', [CastController:: class, 'destory']);