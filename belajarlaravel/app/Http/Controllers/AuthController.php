<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('page.form');
    }

    public function send(request $request)
    {
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        
        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
